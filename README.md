# ONSRun

Program to runs the ONS Simulator and generates automated graphics with the use of Gnuplot scripts
    
OSN Simulator: http://gitlab.com/get-unb/ons

# Usage

    ONSRun -d <results_folder> -j <ons .jar> -f <simulation_file> -s <seeds> -ra <raClasses> -L <minload maxload step> [-trace]

## Arguments: 
    -h                              Print the help (this message) and exit
    -d                              Results directory
    -j                              ONS .jar file
    -f <simulation_file>            XML simulation file
    -s <seeds>                      The simulation seeds (1-25) [separated by space]
    -ra <raClasses>                 Routing Algorithm Classes [separated by space]
    -L <minload maxload step>       For various load sequences, minload maxload step in Erlang
    -trace                          To generate the trace file

# Example

    $ java -jar -j ons.jar -f xml/teste.xml -s 1 2 3 -ra KSP SPV -L 100 600 100 -d results
    $ java -jar -j ons.jar -f xml/teste.xml -s 1 2 3 -ra KSP SPV -L 100 600 100 -d results -trace
    
# Requirements 

    Java 1.8
    Gnuplot 5.0
    ONS 2.0
    
**Note:** This program uses the Java Runtime and only works on Linux