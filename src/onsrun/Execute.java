/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package onsrun;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.logging.Level;
import java.util.logging.Logger;
import static onsrun.Main.trace;
import org.json.simple.parser.ParseException;

/**
 *
 * @author lucasrc
 */
public class Execute implements Runnable {

    private String raClass;

    public Execute(String raClass) {
        this.raClass = raClass;
    }

    @Override
    public void run() {
        Average avg = new Average();
        try {
            Runtime r = Runtime.getRuntime();
            Process process;
            BufferedReader br;
            String saida;
            File file;
            avg.start();
            for (int s = 0; s < Main.seed.length; s++) {
                for (double load = Main.minload; load <= Main.maxload; load += Main.step) {
                    String command = "java -jar " + Main.jarFile + " -f " + Main.simConfigFile + " -s " + Main.seed[s] + " -ra " + raClass + " -l "
                            + load + " -json";
                    if (trace) {
                        command += " -trace";
                    }
                    process = r.exec(command);

                    file = new File(Main.dir + "/" + raClass + "_seed_" + Main.seed[s] + "_" + load);
                    FileWriter fw = new FileWriter(file);
                    BufferedWriter bw = new BufferedWriter(fw);

                    br = new BufferedReader(new InputStreamReader(process.getInputStream()));
                    saida = null;
                    while ((saida = br.readLine()) != null) {
                        //System.out.println(saida); //saida do comando
                        bw.write(saida);
                        bw.newLine();
                    }
                    br.close();
                    bw.close();
                    fw.close();
                    try {
                        avg.addLoad(file, raClass, Main.seed[s], load);
                    } catch (ParseException ex) {
                        Logger.getLogger(Execute.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
                avg.addSeed();
            }
            avg.computeRA(raClass);
            process = r.exec("mkdir "+Main.dir+"/"+raClass+"_seeds");
            process = r.exec(new String[]{"sh", "-c", "mv "+ Main.dir + "/"+ raClass +"_seed* "+ Main.dir + "/"+raClass+"_seeds"});
            System.out.println("\nEnd of class: "+raClass);

        } catch (IOException iOException) {
            iOException.printStackTrace();
        }
    }
}
