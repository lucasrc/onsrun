/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package onsrun;

import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Main {
    
    protected static String simConfigFile = "", jarFile = "", dir = "";
    protected static String[] raClasses;
    protected static int seed[];
    protected static double minload, maxload, step = 1.0;
    protected static boolean trace = false;
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        ArgParsing.parse(args);
        createFolder();
        Execute exec[] = new Execute[raClasses.length];
        Thread t[] = new Thread[raClasses.length];
        for (int i = 0; i < raClasses.length; i++) {
            exec[i] = new Execute(raClasses[i]);
            t[i] = new Thread(exec[i]);
            t[i].start();
            System.out.println("Running the class: "+raClasses[i]);
        }
        int count = 0;
        while(isAlive(t)) {
            System.out.print(".");
            count++;
            if(count == 50) {
                System.out.print("\n");
                count = 0;
            }
            try {
                Thread.sleep(1000);
            } catch (InterruptedException ex) {
                Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        System.out.println("Generating gnuplot script");
        Graphic g = new Graphic();
        g.execute();
        
    }

    private static void createFolder() {
        Runtime r = Runtime.getRuntime();
        try {
            r.exec("rm -rf " + dir);
            r.exec("mkdir " + dir);
        } catch (IOException ex) {
            Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private static boolean isAlive(Thread[] t) {
        for (int i = 0; i < t.length; i++) {
            if (t[i] != null) {
                if (t[i].isAlive()) {
                    return true;
                }
            }
        }
        return false;
    }
}

