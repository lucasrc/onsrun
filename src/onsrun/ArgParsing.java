/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package onsrun;

import java.util.ArrayList;

/**
 *
 * @author lucasrc
 */
public final class ArgParsing {

    private static final String usage = "Usage: ONSRun -d <results_folder> -j <ons .jar> -f <simulation_file> -s <seeds> -ra <raClasses> -L <minload maxload step> [-trace]";

    protected static void printUsage() {
        System.out.println(usage);
        System.exit(0);
    }
    
    private static void printHelp() {
        System.out.println(usage);
        System.out.println("Arguments:\n" 
                + "\t-h                              Print the help (this message) and exit\n"
                + "\t-d                              Results directory\n"
                + "\t-j                              ONS .jar file\n"
                + "\t-f <simulation_file>            XML simulation file\n"
                + "\t-s <seeds>                      The simulation seeds (1-25) [separated by space]\n"
                + "\t-ra <raClasses>                 Routing Algorithm Classes [separated by space]\n"
                + "\t-L <minload maxload step>       For various load sequences, minload maxload step in Erlang\n"
                + "\t-trace                          To generate the trace file\n");
        System.exit(0);
    }

    static void parse(String[] args) {
        boolean flag = false;
        if(args.length == 1 && args[0].equals("-h")) {
            printHelp();
        }
        String comand = "";
        for (String arg : args) {
            comand += arg + " ";
        }
        int flags = 0, loads = 0, seeds = 0, files = 0, ras = 0, traces = 0, jar = 0, folder = 0;
        folder += comand.split("-d ", -1).length-1;
        jar += comand.split("-j ", -1).length-1;
        files += comand.split("-f ", -1).length-1;
        seeds += comand.split("-s ", -1).length-1;
        ras += comand.split("-ra ", -1).length-1;
        loads += comand.split("-L ", -1).length-1;
        traces += comand.split("-trace", -1).length-1;
        flags += loads + seeds + files + ras + traces + jar + folder;
        if (flags < 6 || flags > 7) {
            printUsage();
        } else {
            if (loads > 1) {
                System.out.println("Were declared two flags '-L'");
                printUsage();
            }
            if (folder > 1) {
                System.out.println("Were declared two flags '-d'");
                printUsage();
            }
            if (files > 1) {
                System.out.println("Were declared two flags '-f'");
                printUsage();
            }
            if (seeds > 1) {
                System.out.println("Were declared two flags '-s'");
                printUsage();
            }
            if (ras > 1) {
                System.out.println("Were declared two flags '-ra'");
                printUsage();
            }
            if (traces > 1) {
                System.out.println("Were declared two flags '-trace'");
                printUsage();
            }

        }
        if(comand.split("-trace").length-1 == 1){
            Main.trace = true;
        }
        try {
            Main.seed = stringToInt(getIntArgs(comand.split("-s ")[1].split(" ")));
            Main.dir = comand.split("-d ")[1].split(" ")[0];
            Main.jarFile = comand.split("-j ")[1].split(" ")[0];
            Main.simConfigFile = comand.split("-f ")[1].split(" ")[0];
        } catch (Exception e) {
            printUsage();
        }
        try {
            if(comand.split("-ra ", -1).length-1 == 1) {
                Main.raClasses = getStringArgs(comand.split("-ra ")[1].split(" "));
            }
            if(comand.split("-L ", -1).length-1 == 1) {
                Main.minload = Double.parseDouble(comand.split("-L ")[1].split(" ")[0]);
                Main.maxload = Double.parseDouble(comand.split("-L ")[1].split(" ")[1]);
                Main.step = Double.parseDouble(comand.split("-L ")[1].split(" ")[2]);
            }
        } catch (Exception e) {
            printUsage();
        }
    }

    private static int[] stringToInt(String[] vstring) {
        int[] n = new int[vstring.length];
        for (int i = 0; i < vstring.length; i++) {
            n[i] = Integer.parseInt(vstring[i]);
            if(n[i] < 1 || n[i] > 25) {
                System.out.println("The seed should be between 1 and 25");
                printUsage();
            }
            
        }
        return n;
    }

    private static String[] getIntArgs(String[] vstring) {
        String[] s = null;
        boolean flag = false;
        ArrayList<String> aux = new ArrayList<>();
        for (int i = 0; i < vstring.length; i++) {
            if(isInt(vstring[i])) {
                flag = true;
                aux.add(vstring[i]);
            } else {
                if(flag) {
                    break;
                } else {
                    printUsage();
                }
            }
        }
        s = new String[aux.size()];
        for (int i = 0; i < aux.size(); i++) {
            s[i] = aux.get(i);
        }
        return s;
    }

    private static String[] getStringArgs(String[] vstring) {
        String[] s = null;
        boolean flag = false;
        ArrayList<String> aux = new ArrayList<>();
        for (int i = 0; i < vstring.length; i++) {
            if(!vstring[i].contains("-")) {
                flag = true;
                aux.add(vstring[i]);
            } else {
                if(flag) {
                    break;
                } else {
                    printUsage();
                }
            }
        }
        s = new String[aux.size()];
        for (int i = 0; i < aux.size(); i++) {
            s[i] = aux.get(i);
        }
        return s;
    }
    
    public static boolean isInt(String s) {
        try {
            Integer.parseInt(s);
            return true;
        } catch (NumberFormatException ex) {
            return false;
        }
    }
    
    
}
